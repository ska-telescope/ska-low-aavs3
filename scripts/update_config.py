# -*- coding: utf-8 -*-
"""
A script to update the configuration specification for AAVS3.

It reads in the existing configuration specification
(which contain inaccuracies that this script needs to correct)
and a CSV file containing a summary of AAVS3 short functional test (SFT) results.

It cross-checks the SFT results against the known configuration,
and corrects the inaccurate aspects based on the SFT results.

This is most likely a one-shot script that will never be run again.
But let's check it in anyhow, as a record of what was done.
"""

import copy
import csv
import urllib.request
import yaml

# preADU channel mapping from
# https://confluence.skatelescope.org/display/SE/REL-707%3A+preADU+Control+Software+Release+Documentation
# published there as
#     channel: (fibre port, polarity)
# but here rearranged to
#     fibre_port: (y_channel, x_channel)
preadu_channel_map = {
    16: (14, 15),
    15: (12, 13),
    14: (10, 11),
    13: (8, 9),
    12: (7, 6),
    11: (5, 4),
    10: (3, 2),
    9: (1, 0),
    8: (30, 31),
    7: (28, 29),
    6: (26, 27),
    5: (24, 25),
    4: (23, 22),
    3: (21, 20),
    2: (19, 18),
    1: (17, 16),
}

# Current platform spec to be updated
f = urllib.request.urlopen("https://gitlab.com/ska-telescope/ska-low-aavs3/-/raw/8a9ca5da19efb026e5f556dc7aab278e38f63327/helmfile.d/values/aavs3.yaml")
platform_config = yaml.load(f, Loader=yaml.SafeLoader)
antenna_config = platform_config["platform"]["array"]["station_clusters"]["a1"]["stations"]["1"]["antennas"]

new_antenna_config = {}

def combine_config(antenna, row):
    combined = copy.deepcopy(antenna)
    combined["tpm"] = row["TPM"]
    if "masked" in combined:
        del combined["masked"]  # We'll recompute this
    fibre_port = int(row["fibre port"])
    y_channel, x_channel = preadu_channel_map[fibre_port]

    assert y_channel == int(row["chanY"])
    assert x_channel == int(row["chanX"])

    combined["tpm_y_channel"] = y_channel
    combined["tpm_x_channel"] = x_channel

    assert row["pass/fail"] in ["PASS", "FAIL"]
    if row["pass/fail"] == "FAIL":
        combined["masked"] = True

    return combined


# AAVS3 short functional test spreadsheet
with open('scripts/aavs3_sft.csv') as csvfile:
    reader = csv.DictReader(csvfile)

    for row in reader:
        smartbox = int(row["SMARTBox"])
        smartbox_port = int(row["FEM"])

        for antenna_id, antenna_spec in antenna_config.items():
            if int(antenna_spec["smartbox"]) != smartbox:
                continue
            if int(antenna_spec["smartbox_port"]) != smartbox_port:
                continue
            # This "ant" prefix is undesired but it stops yaml 1.1
            # from interpreting the zero-padded antenna ID as octal.
            # And I'm only padding with zeros
            # because I want the yaml to be output in antenna order.
            antenna_key = f"ant{int(antenna_id):03}"
            new_antenna_config[antenna_key] = combine_config(antenna_spec, row)
            del antenna_config[antenna_id]
            break

    assert not antenna_config, f"Oh noes! I still have an unmapped antenna: {antenna_config}"

    print(yaml.dump(new_antenna_config))
