include .make/base.mk

########################################
## Landing website
########################################

install-pelican:
	python3 -m pip install pelican tzdata

site-build:
	cd site; pelican content/

site-build-for-publish:
	cd site; pelican --settings publishconf.py content/

site-launch:
	cd site; pelican --listen


########################################
## OCI
########################################

include .make/oci.mk

oci-pre-build: install-pelican site-build-for-publish


########################################
# k8s / Helm
########################################

include .make/k8s.mk
include .make/helm.mk


########################################
# TELMODEL
########################################

include .make/tmdata.mk


########################################
# Deployment stuff
########################################

vault-login:
	vault token lookup -address https://vault.skao.int \
	|| vault login -address https://vault.skao.int -method=oidc

deploy-aavs3: vault-login
	helmfile sync --selector name=ska-low-aavs3

deploy-mccs:
	helmfile sync --selector name=ska-low-mccs
