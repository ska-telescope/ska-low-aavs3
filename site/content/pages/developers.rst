Information for software developers
###################################

Source
~~~~~~

The AAVS3 software deployment is defined in the `ska-low-aavs3`_ Gitlab repository,
and deployed through the AAVS3 server's kubernetes API.

The jupyterhub environment is defined in the `ska-low-jupyter`_ Gitlab repository.

.. _ska-low-aavs3: https://gitlab.com/ska-telescope/ska-low-aavs3
.. _ska-low-jupyter: https://gitlab.com/ska-telescope/ska-low-jupyter
