# AAVS3 (and later station) short functional tests

## 1. FEMs-off tests (not actually FEMs off tests)
* Check preADU functionality
  * Assign a range of attenuation values across all channels
  * We probably need sky in the antennas for this, noise will be too low

## 2. Channel by channel tests
This replicates what our STF currently does. Channel by channel:
* record ADC power
* record FEM currents, state (and temperatures?)
* Capture bandpass
  * Across the whole station and check for EMI in other channels
  * Use a 1 second integration

## 2.1 Apply initial cable delays based on as-built cable lengths

## 3. All-channel tests
1. Bring all up and equalise powers
   * Check for attenuators at limit, eliminate dead channels
2. Capture spectrum
   * 1 second integration
   * Use some stats to find outliers in shape, excluding known RFI bands
4. Capture a correlation matrix with the DAQ at 130MHz (or some other RFI-free channel)
5. Do we want to frequency sweep as well? No.

## 4. Closure phase and amplitude?
Compute closure phase and amplitude from correlation matrix, compare to expected (model)
* Produces 30,000 closures, but you can use a non-redundant subset
* Can do this with the correlation matrix captured in 3.
* Doesn’t require calibration
* Identifies outliers that cannot be used for calibration

## 5. Calibration tests
* For a certain set of frequencies:
  * Calculate a calibration solution using SDP routines and sky model
  * Inspect amplitudes to identify pol swaps, misplaced antennas other outliers
  * Apply calibration solution to the data
  * Compare magnitude and phase to model across XX, XY, YX, YY
* Compare calibration solutions across frequencies
  * use this to verify/refine cable delays
* Apply cable delays and calibration solution to TPMs and re-acquire
* Confirms
  * Hardware to be functional
  * Built as planned
  * Configuration is correct

## 6. Checking that EEP antenna labelling is correct
* Compare measured calibrated visibility vs predicted visibility
* Needs to be done once against the sky, not for every station

## 7. Stability tests
* Every 5 minutes - 5 hours or more
  * Correlation matrix
  * Station beam samples
* Look at visibilities as a function of time
* Compare the model and the visibilities over time

## 8. Station sensitivity
* https://skaoffice.jamacloud.com/perspective.req#/items/906350?projectId=335
* Take correlation matrixes integrated over adjacent seconds
* Calibrate separately
* Create sky map of the two
* Subtract the two to determine noise

## 9. Beamforming
* Pick a pulsar, want to measure SNR while pointing at it
  * Capture beamformed complex voltages across as many channels as possible
* Use full station first
* Then take antennas out - you should see a linear reduction in SNR
